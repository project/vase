<?php

/**
 * Hook implementations and shared functions.
 */

/**
 * Implements hook_field_formatter_info_alter().
 */
function vase_field_formatter_info_alter(array &$info) {
  foreach ($info as $name => &$formatter) {
    $formatter['settings']['vase'] = array(
      'original_module' => $formatter['module'],
      'offset' => '0',
      'length' => '0',
    );
    $formatter['module'] = 'vase';
  }
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function vase_field_formatter_settings_summary(array $field, array $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings']['vase'];

  return t('!original_summary (Offset: !offset. Length: !length)', array(
    '!original_summary' => module_invoke($settings['original_module'], 'field_formatter_settings_summary', $field, $instance, $view_mode),
    '!offset' => $settings['offset'],
    '!length' => $settings['length'] == 0 ? t('unlimited') : $settings['length'],
  ));
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function vase_field_formatter_settings_form(array $field, array $instance, $view_mode, array $form, array &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings']['vase'];

  $elements['vase'] = array(
    '#type' => 'fieldset',
    '#title' => t('Selection of values to display'),
    '#tree' => TRUE,
  );
  $elements['vase']['original_module'] = array(
    '#type' => 'value',
    '#value' => $settings['original_module'],
  );
  $elements['vase']['offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Start'),
    '#default_value' => $settings['offset'],
    '#element_validate' => array('element_validate_integer'),
    '#size' => 9,
    '#required' => TRUE,
  );
  $elements['vase']['length'] = array(
    '#type' => 'textfield',
    '#title' => t('Length'),
    '#description' => t('Enter 0 for <em>unlimited</em>.'),
    '#default_value' => $settings['length'],
    '#element_validate' => array('element_validate_integer'),
    '#size' => 9,
    '#required' => TRUE,
  );
  if (module_hook($settings['original_module'], 'field_formatter_settings_form')) {
    $function = $settings['original_module'] . '_field_formatter_settings_form';
    $elements = array_merge($function($field, $instance, $view_mode, $form, $form_state), $elements);
  }

  return $elements;
}

/**
 * Implements hook_field_formatter_view().
 */
function vase_field_formatter_view($entity_type, $entity, array $field, array $instance, $langcode, array $items, $display) {
  $settings = $display['settings']['vase'];

  $length = $settings['length'] == 0 ? NULL : $settings['length'];
  $items = array_slice($items, $settings['offset'], $length);

  return module_invoke($settings['original_module'], 'field_formatter_view', $entity_type, $entity, $field, $instance, $langcode, $items, $display);
}